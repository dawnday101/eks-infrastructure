provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "local" {
    path = "vpc/terraform.tfstate"
  }
}

module "vpc" {
  source = "../../infrastructure/modules/vpc"

  env             = "prod"
  azs             = ["us-east-1a", "us-east-1b"]
  private_subnets = ["10.216.17.0/24", "10.216.16.0/24"]
  public_subnets  = ["10.216.19.0/24", "10.216.18.0/24"]

  private_subnet_tags = {
    "kubernetes.io/role/internal-elb" = 1
    "agreement"                       = "oir9950"
    "Name"                            = "oir9950-prod-us-east-1b-priv"    
    "kubernetes.io/cluster/prod-demo" = "owned"
  }

  public_subnet_tags = {
    "kubernetes.io/role/elb"          = 1
    "kubernetes.io/cluster/prod-demo" = "owned"
    "agreement"                       = "oir9950"
    "Name"                            = "oir9950-prod-us-east-1b-pub"
  }
}