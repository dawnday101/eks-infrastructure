variable "subnet_ids" {
    default   = ["subnet-0cb0b9093cb7d2ed5","subnet-05b39fa4b88a11dc0"]
    type      = list(string)
    description = "subnets for nodes"
}

variable "region" {
    type      = string
    description = "AWS region in create cluster"
}