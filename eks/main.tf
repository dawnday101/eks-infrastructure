provider "aws" {
  region = var.region
}

terraform {
  backend "local" {
    path = "eks/terraform.tfstate"
  }
}

module "eks" {
  source = "../../eks-infra/modules/eks"
  
  eks_name    = "usda"
  env         = "dev"
  eks_version = "1.26"
  subnet_ids  = var.subnet_ids

  node_groups = {
    worker = {
      name               = "usda-workers"
      capacity_type      = "ON_DEMAND"
      instance_types     = ["t3a.large"]

      scaling_config = {
        desired_size     = 2
        max_size         = 3
        min_size         = 2
      } 
    }
  }
}

